const float TEMP = 24.2;                  // 24 was marginally to cold >.>
const float MARGIN = 0.3;                 // was 0.5
const int CHECK_TIME = 3600;              // s in an hour

int thermistor_value;
float volt;
float temp;
int warm;                                 // Do we want the radiator warm?
int old_warm;
int spent_time = 0;

void send_to_thermostat(int signal) {
  digitalWrite(signal, HIGH);             // Turn on
  delay(300);
  digitalWrite(signal, LOW);
}

void setup() {
  old_warm = 2;                           // Forced activation first loop
  pinMode(13, OUTPUT);                    // Controller - on
  pinMode(12, OUTPUT);                    // Controller - off
  pinMode(A0, INPUT);                     // Thermistor

  //Serial.begin(9600);                     // Debuging
}

void loop() {
  // Redo termistor circuit for linear and more precise input
  volt = analogRead(0) * (5.0/1024);
  temp = 19.3 * volt - 29.7;              // Should be about right ~ish

  if (spent_time >= CHECK_TIME) {
    old_warm = 2;
    spent_time = 0;
  }

  if (temp < (TEMP - MARGIN))             // We don't want it to ocillate at 24
    warm = 1;

  if (temp > (TEMP + MARGIN))
    warm = 0;

  if (warm != old_warm) {
    if (warm) {
      send_to_thermostat(13);
    } else {
      send_to_thermostat(12);
    }
    old_warm = warm;
  }


  /*
  Serial.print(warm);                     // Debuging
  Serial.print(" old: ");
  Serial.print(old_warm);
  Serial.print(" - ");
  Serial.println(temp);
  */
  delay(4000);                            // We don't need to check that often
  spent_time += 4;
}
