# Purpose
I needed a way to control the temperature in my appartment and since my main heat source is a simple radiator I want to be able to control this radiator. All this on a very low budget but with most of the parts laying around.

Plan: Buy a wirelessly controlled power switch to put between the radiator and wall jack. Set up an arduino with a thermistor to messure the temperature in the room and control the remote for the power switch.

wireless power switch used: https://www.kjell.com/se/sortiment/el-verktyg/smarta-hem/433-mhz/fjarrstrombrytare/utanpaliggande-brytare/cleverio-mini-fjarrstrombrytare-3000-w-4-pack-p51044

arduino used: Arduino UNO